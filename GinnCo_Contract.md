# GinnCo. Contract

GinnCo. welcomes you to your next and likely last engagment. You have been selected as an elite member of a GinnCo. Your new task is to carry out GinnCo. directives and project GC power into Faerûn. 

## Your Duty
Is first and foremost to your Party. A Party is only effective if its members can trust their comrades. **You bound not to lie to, steal from, or unreasonably harm your Party's members.** Your second duty is to project GinnCo. power into Faerûn. 

**Your goals are to cooperate with your party members and increase the reputation of GinnCo through deeds and action of repute.**

### Loot Rules
GinnCo. uses *Need versus Greed* to govern loot distribution.

The Party will keep tasteful track of magic items. Remember to not be a pig. For example, if you have more than 2 powerful magic items than another player you should think carefully about your participation in the next loot roll. According to honor, a good player would either pass on the loot roll entirely or volunteer to give one of the roll's losers a magic item that would benefit them. Negotiation and trades for the good of the group using gold, magic, or other obligations are acceptable in order to resolve conflicts of interest.

*Needed* items must optimize a feature of your class or role. Needed items must be ones that would be often used for the good of the group. For example if a pair of elvish slippers dropped and you as a lightly armored fighter might use them to help your stealh, but there is another member of the team that relies on stealth day in day out Greed or Pass on the roll.

>##### GinnCo. Rules
> GinnCo. Party members operate within a code:
>**The GC Code**
---
> * Protect yourself and the party from harm.
> * Leave no Party Member behind.
> * Honor the rules of the Loot System.
> * Do the best of your ability, do not intentionally make poor strategic or tactical choices.
> * When walking in the world, bother no one. If someone bothers you, ask them to stop. If they do not stop, destroy them.

If you would like to modify these rules, appeal to Ginn at GinnCo HQ Waterdeep.

<div style='margin-top:50px'></div>

### Roles of the Party:
* **Faction Agent** - Political office representing the interests of GinnCo. in pursuing the party's mission objectives. Has the power to call for a vote of no confidence in the Leader. 
* **Leader** - Voted into office democratically by the party and bestowed with a mandate. Typically this person has the highest Charisma in the party. Ties are resolved by a roll-off and then by the Faction Agent.
* **Treasurer** - Keeper of the Adventure Ledger. This person must be Lawful Good or closest too it.
* **Cartographer** - Keeper of the Adventure Map. Responsible for the informational integrity of the Party's maps.
* **Secretary** - Takes notes about the Adventure, writes down riddles. This person must have good handwriting.
* **Controller** - Takes and keeps initiative order and informs the Order-of-Combat.

Characters are allowed to hold multiple offices.

### Resolving Conflicts or Member Transgressions:
Party Members may nominate another Member(s) for *default* status for gross or repeated code or Party infringment. A nominating motion is commenced with a second and raified with a majority vote. Each Party Member is given a vote. 

Appeals can be made to Ginn magically or physcially. *Default* status results in total Party divestiture and forfeture of all treasure, magical or otherwise, gained while on the Party's Adventure. Those found in *default* status are provided a sword to regain their honor.

<div style='margin-top:80px'></div>


###   
Sign Here
<div style='margin-top:40px'></div>

###   
Print Here
<div style='margin-top:40px'></div>


  
### Ginn Glitterspike
Ginn Glitterspike (yes thats's his penis's name)

<img src='http://i.imgur.com/hMna6G0.png' style='position:absolute;bottom:50px;right:3px;width:200px' />

<!-- <div class='pageNumber'>1</div> -->
<div class='footnote'>GinnCo. | Adventuring Contract</div>

