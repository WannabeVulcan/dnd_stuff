# GinnCo. Contract

GinnCo. welcomes you to your next and possibly last engagment. You have been selected as an elite member of a GinnCo. league to carry out GC directives and project our power into the realm. Think **purple** lightsaber - rules are meant to be broken or followed if you are LG.

## Your Duty
Is first and foremost to each other. A group is only effecitve if they can rely on their commrades. YOU WILL NOT LIE, CHEAT OR STEAL FROM YOUR COMPANIONS. Your seconded duty is to project GinnCo. power into the realm. 

**Your goal is to increase the reputation of GinnCo through deeds and action.



### Loot Rules
Need and Greed rolls govern loot management.

With a loose tracking of quantity of magic items. Don't be a pig. For example, If you have more that 2 rare or greater items than other player you either pass on the need or if you win agree to give them one of your other magic items.

Need items optimized for your class/role. Must be an item you would use all the time. For example if a pair of move silently slippers dropped and you as a leather armored fighter might use them to help you stealh, but there is another member of the team that relies on stealth day in day out. Pass on the need role and let that memeber have the item.
### Helping out

Help your teammates. Work for the parties best interest.

***
<div style='margin-top:40px'></div>



>##### Ginn Co. Rules 
> The rules are simple and direct. Work as a group for the group. If you are found to be selfish you will be exposed and expelled.
>
> Code
> * Honor loot rules
> * Help your teammates
> * Leave no one behind
> * Do not cause harm to your fellow adventurers
> * Don't be a Lemon. Do the best you can, don't intentionally make poor choices
>
> If you want to append these rules appeal to Ginn at GinnCo HQ Waterdeep


<div style='margin-top:50px'></div>


### Faction Agent
Represents the will of GinnCo. in pursuing the party's mission objectives.

### Roles in the party
* **Leader** - Highest Charisma in the party. Tie is a roll off. Defers to the party, but if a concensious can't be reached must step up and make the call.
* **Treasurer** - Loot holder and keeper of the ledger. Must be Lawful Good or closest too it
* **Cartographer** - Sexy map maker
* **Secretary** - Takes notes, writes down riddles, has good handwriting
* **Controller** - Calls initiative order and directs order of combat
* **Agent** - Faction agent responsible for reporting back to GinnCo. Has the power to call for a vote of no confidence in the leader. 
* **Peon** - Work work

### Legal Junk
You shall not work against your own team mates. (see being a Lemon) This includes imploying physical or magical means to obfiscate your person and what you represent. 


### Aggrement Default
The party may vote members into default status for gross or repeated rule infringment. Nominating motion with a second followed by a majority vote. 

Appeals can be made to Ginn magically or physcially. Default status results in party divestiture and forfeture of all treasure gained as part of the team. Including all magical items.
<div style='margin-top:75px'></div>


###   
Sign Here
<div style='margin-top:40px'></div>

###   
Print Here
<div style='margin-top:50px'></div>


  
### Ginn Glitterspike
Ginn Glitterspike (yes thats's his penis's name)

<img src='http://i.imgur.com/hMna6G0.png' style='position:absolute;bottom:50px;right:3px;width:190px' />

<div class='pageNumber'>1</div>
<div class='footnote'>PART 1 | FANCINESS</div>




\page

# Appendix

### Not quite Markdown
Although the Homebrewery uses Markdown, to get all the styling features from the PHB, we had to get a little creative. Some base HTML elements are not used as expected and I've had to include a few new keywords.

___
* **Horizontal Rules** are generally used to *modify* existing elements into a different style. For example, a horizontal rule before a blockquote will give it the style of a Monster Stat Block instead of a note.
* **New Pages** are controlled by the author. It's impossible for the site to detect when the end of a page is reached, so indicate you'd like to start a new page, use the new page snippet to get the syntax.
* **Code Blocks** are used only to indicate column breaks. Since they don't allow for styling within them, they weren't that useful to use.
* **HTML** can be used to get *just* the right look for your homebrew. I've included some examples in the snippet icons above the editor.



```
```


### Images
Images must be hosted online somewhere, like imgur. You use the address to that image to reference it in your brew. Images can be included 'inline' with the text using Markdown-style images. However for background images more control is needed.

Background images should be included as HTML-style img tags. Using inline CSS you can precisely position your image where you'd like it to be. I have added both a inflow image snippet and a background image snippet to give you exmaples of how to do it.



### Crediting Me
If you'd like to credit The Homebrewery in your brew, I'd be flattered! Just reference that you made it with The Homebrewery.



<div class='pageNumber'>2</div>
<div class='footnote'>PART 2 | BORING STUFF</div>




