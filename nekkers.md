<img src='https://i.imgur.com/ZFV3CP7.png' />

<!-- style='top:50px;width:280px;height:380px;' --> 

## Nekkers
Awful stench and gore are usually found before a Nekker rends an adventurer with their claws. Distant and mutated cousins of giants, these small creatures dwell in caves near roads. Waylaying travelers and merchants alike. Relieiving people of their flesh is a Nekker's favorite pasttime.

<!-- [placeholder summary] Terrible minor ogroids that hunt in packs. [placeholder summary] Terrible minor ogroids that hunt in packs. [placeholder summary] Terrible minor ogroids that hunt in packs. errible minor ogroids that hunt in packs. errible minor ogroids that hunt in packs. errible minor ogroids that hunt in packs. errible minor ogroids that hunt in packs. errible minor ogroids that hunt in packs. -->


___ 
> ## Nekker
>*Small Giant, chaotic evil*
> ___
> - **Armor Class** 13 (natural armor)
> - **Hit Points** 8 (1d12 + 1)
> - **Speed** 25ft.,  burrow 15ft
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|14 (+2)|12 (+1)|12 (+1)|8 (-1)|10 (+0)|6 (-2)|
>___
> - **Skills** Perception +2
> - **Senses** darkvision 60ft., passive Perception 12
> - **Languages** Giant
> - **Challenge** 1/4 (50 XP)
> ___
> ***Pack Tactics.*** The nekker has advantage on attack rolls against a creature if at least one of the nekker's allies is within 5 feet of the creature and the ally is not incapacitated.
> ### Actions
> **Claw** *Melee Weapon Attack:* +4 to hit, reach 5ft., one target. *Hit* 5 (1d6 + 2) 



```
```

___
> ## Nekker Warrior
>*Small Giant, chaotic evil*
> ___
> - **Armor Class** 16 (natural armor)
> - **Hit Points** 43 (6d12 + 1)
> - **Speed** 25ft.,  burrow 15ft
>___
>|STR|DEX|CON|INT|WIS|CHA|
>|:---:|:---:|:---:|:---:|:---:|:---:|
>|16 (+3)|14 (+2)|12 (+1)|15 (+3)|10 (+0)|6 (-2)|
>___
> - **Skills** Perception +4
> - **Senses** darkvision 60ft., passive Perception 14
> - **Languages** Giant
> - **Challenge** 2 (450 XP)
> ___
> **Pack Tactics.** The nekker warrior has advantage on attack rolls against a creature if at least one of the nekker warrior's allies is within 5 feet of the creature and the ally is not incapacitated.
> ### Actions
> **Multiattack** The nekker warrior makes two claw attacks.

> **Claw** *Melee Weapon Attack:* +6 to hit, reach 5ft., one target. *Hit* 5 (1d6 + 3) 




### Nekker
The basic Nekker

### Nekker Warrior
The upgraded Nekker that is smarter and meaner.The upgraded Nekker that is smarter and meaner.The upgraded Nekker that is smarter and meaner.The upgraded Nekker that is smarter and meaner.The upgraded Nekker that is smarter and meaner.The upgraded Nekker that is smarter and meaner.The upgraded Nekker that is smarter and meaner.The upgraded Nekker that is smarter and meaner.The upgraded Nekker that is smarter and meaner.



<img src='https://vignette.wikia.nocookie.net/witcher/images/2/25/Nekker_Warrior.png/revision/latest?cb=20160515191705' />

<div class='pageNumber'>1</div>
<div class='footnote'>WITCHER 3 | MONSTERS</div>

